# Metadata - [![version](https://img.shields.io/npm/v/@typemon/metadata.svg)](https://www.npmjs.com/package/@typemon/metadata) [![license](https://img.shields.io/npm/l/@typemon/metadata.svg)](https://gitlab.com/monster-space-network/typemon/metadata/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/metadata/dev/typescript.svg) [![gitlab-pipeline](https://gitlab.com/monster-space-network/typemon/metadata/badges/master/pipeline.svg)](https://gitlab.com/monster-space-network/typemon/metadata/-/pipelines) [![coverage](https://gitlab.com/monster-space-network/typemon/metadata/badges/master/coverage.svg)](https://gitlab.com/monster-space-network/typemon/metadata/-/graphs/master/charts)
- Provides highly abstracted metadata management functions
- Support inheritance and parameter metadata
- Fully integrated with decorators



## Installation
```
$ npm install @typemon/metadata reflect-metadata
```

### [TypeScript](https://github.com/Microsoft/TypeScript)
Configure metadata and decorator options.
```json
{
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
}
```

### [Reflect Metadata](https://github.com/rbuckton/reflect-metadata)
Import only once at the application entry point.
```typescript
import 'reflect-metadata';
```



## Usage
I don't think I need an explanation.
```typescript
const metadata: Metadata = Metadata.of({
    target,
    propertyKey?,
    parameterIndex?,
});

metadata.set(key, value);
metadata.get(key, value);
metadata.delete(key);
```

This is recommended for a one-time operation.
```typescript
Metadata.set({
    target,
    propertyKey?,
    parameterIndex?,
    key,
    value,
});
```



## Parameter Metadata
In the same way, you can easily manage parameter metadata as well.
- Inheritance is supported just like regular metadata.
- If possible, the `design:type` metadata is set.
```typescript
@Decorator()
class Parent {
    public constructor(
        public readonly parameter: string,
    ) { }
}

@Decorator()
class Child extends Parent { }

const child: Metadata = Metadata.of({
    target: Child,
    parameterIndex: 0,
});
const parent: Metadata = Metadata.of({
    target: Parent,
    parameterIndex: 0,
});

expect(child.parent).toBe(parent);
expect(child.get('design:type')).toBe(String);
expect((): void => child.getOwn('design:type')).toThrow(MetadataDoesNotExistError);
```



## Decorator
You can easily create decorators, and basically decorate them everywhere.
```typescript
function Decorator(...parameters: any): Metadata.Decorator {
    return Metadata.decorator((metadata: Metadata): void => {
        ...
    });
}

@Decorator(key, value)
class Example {
    @Decorator(key, value)
    public static property: unknown;

    @Decorator(key, value)
    public static method(
        @Decorator(key, value)
        parameter: unknown,
    ): void { }

    @Decorator(key, value)
    public property: unknown;

    public constructor(
        @Decorator(key, value)
        parameter: unknown,
    ) { }

    @Decorator(key, value)
    public method(
        @Decorator(key, value)
        parameter: unknown,
    ): void { }
}
```

You can limit the type.
```typescript
function Decorator(...parameters: any): ClassDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        ...
    });
}
```

You can combine types.
```typescript
function Decorator(...parameters: any): PropertyDecorator & MethodDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        ...
    });
}
```

You can also decorate dynamically.
```typescript
Metadata.decorate({
    target,
    propertyKey?,
    parameterIndex?,
    decorators,
});
```
