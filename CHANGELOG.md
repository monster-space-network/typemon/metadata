# [1.0.0-rc.4](https://gitlab.com/monster-space-network/typemon/metadata/compare/1.0.0-rc.3...1.0.0-rc.4) (2021-01-26)


### Bug Fixes

* 심볼 문제 해결 ([989c231](https://gitlab.com/monster-space-network/typemon/metadata/commit/989c23142e0f810e831e9097e331d32916ba655a))


### Features

* 부모 기능 추가 ([1002902](https://gitlab.com/monster-space-network/typemon/metadata/commit/10029023b0ef5bc788d58138d35fe0898a941e12))



# [1.0.0-rc.3](https://gitlab.com/monster-space-network/typemon/metadata/compare/1.0.0-rc.2...1.0.0-rc.3) (2020-12-27)


### Features

* 메타데이터 키 유형 변경 ([da6f788](https://gitlab.com/monster-space-network/typemon/metadata/commit/da6f78856c2e6377fca98d96786aaa4acd35d3a4))
* 오류 메시지 변경 ([6325422](https://gitlab.com/monster-space-network/typemon/metadata/commit/632542248b1396990a385db61f0383294574990e))


### Reverts

* Revert "chore(readme): 데코레이터 예제 코드 추가" ([da01309](https://gitlab.com/monster-space-network/typemon/metadata/commit/da01309bb6ad6c8a8532fb769f01c94605107f09))



# [1.0.0-rc.2](https://gitlab.com/monster-space-network/typemon/metadata/compare/1.0.0-rc.1...1.0.0-rc.2) (2020-12-25)


### Features

* **metadata-key:** 디자인 반환 유형 키 추가 ([7f488e8](https://gitlab.com/monster-space-network/typemon/metadata/commit/7f488e80ab2868211bf9c92ebfe87a783efce1b8))
* **metadata-manager:** is, isNot 메서드 추가 ([beb0197](https://gitlab.com/monster-space-network/typemon/metadata/commit/beb0197564c6d6db0c7ec95d2db5be6f9503d357))


### Reverts

* Revert "chore(tsconfig): types 구성 변경" ([5ec8966](https://gitlab.com/monster-space-network/typemon/metadata/commit/5ec89666b1ce57b7f7a0cd62597a29aaf2c53763))



# [1.0.0-rc.1](https://gitlab.com/monster-space-network/typemon/metadata/compare/1.0.0-rc.0...1.0.0-rc.1) (2020-12-21)


### Bug Fixes

* 누락된 수출 항목 추가 ([5e628cf](https://gitlab.com/monster-space-network/typemon/metadata/commit/5e628cf2a5a757464032a399fe3e639917c22dd0))


### Features

* **metadata-key:** 키 유형 및 값 변경 ([02488d8](https://gitlab.com/monster-space-network/typemon/metadata/commit/02488d85446bf4aa0535e086487240761457feb3))



