import { Check } from '@typemon/check';

import { MetadataKey } from './metadata-key';
import { MetadataOwner } from './metadata-owner';
import { ClassMetadata } from './class-metadata';
import { ClassParameterMetadata } from './class-parameter-metadata';

export class MetadataManager {
    private getParent(target: object): null | object {
        const targetConstructor: Function = Check.isFunction(target)
            ? target
            : target.constructor;
        const parentConstructor: Function = Object.getPrototypeOf(targetConstructor);
        const parent: null | object = Check.equal(parentConstructor, Function.prototype)
            ? null
            : Check.isFunction(target)
                ? parentConstructor
                : parentConstructor.prototype;

        return parent;
    }

    private createMetadata(target: object, propertyKey?: string | symbol): ClassMetadata {
        const parent: null | object = this.getParent(target);
        const parentMetadata: null | ClassMetadata = Check.isNull(parent)
            ? null
            : this.getMetadata(parent, propertyKey);

        return new ClassMetadata({
            owner: {
                target,
                propertyKey,
            },
            parent: parentMetadata,
        });
    }

    private createParameterMetadata(target: object, propertyKey: undefined | string | symbol, parameterIndex: number): ClassParameterMetadata {
        const parent: null | object = this.getParent(target);
        const parentMetadata: null | ClassParameterMetadata = Check.isNull(parent)
            ? null
            : this.getParameterMetadata(parent, propertyKey, parameterIndex);

        return new ClassParameterMetadata({
            owner: {
                target,
                propertyKey,
                parameterIndex,
            },
            parent: parentMetadata,
        });
    }

    private getMetadata(target: object, propertyKey?: string | symbol): ClassMetadata {
        if (Reflect.hasOwnMetadata(MetadataKey.METADATA, target, propertyKey!)) {
            return Reflect.getOwnMetadata(MetadataKey.METADATA, target, propertyKey!);
        }

        const metadata: ClassMetadata = this.createMetadata(target, propertyKey);

        Reflect.defineMetadata(MetadataKey.METADATA, metadata, target, propertyKey!);

        return metadata;
    }

    private getParameterMetadata(target: object, propertyKey: undefined | string | symbol, parameterIndex: number): ClassParameterMetadata {
        const metadata: ClassMetadata = this.getMetadata(target, propertyKey);
        const parameterMetadataList: Array<undefined | ClassParameterMetadata> = metadata.hasOwn(MetadataKey.METADATA_LIST)
            ? metadata.getOwn(MetadataKey.METADATA_LIST)
            : [];
        const parameterMetadata: ClassParameterMetadata = parameterMetadataList[parameterIndex] ?? this.createParameterMetadata(target, propertyKey, parameterIndex);

        parameterMetadataList[parameterIndex] = parameterMetadata;

        metadata.set(MetadataKey.METADATA_LIST, parameterMetadataList);

        return parameterMetadata;
    }

    public is(value: unknown): value is ClassMetadata {
        return Check.instanceOf(value, ClassMetadata);
    }

    public isNot<Value>(value: Value): value is Exclude<Value, ClassMetadata> {
        return Check.isFalse(this.is(value));
    }

    public of(owner: MetadataOwner): ClassMetadata {
        return Check.isUndefined(owner.parameterIndex)
            ? this.getMetadata(owner.target, owner.propertyKey)
            : this.getParameterMetadata(owner.target, owner.propertyKey, owner.parameterIndex);
    }

    public hasOwn(data: MetadataOwner & MetadataManager.Key): boolean {
        return this.of(data).hasOwn(data.key);
    }
    public hasNotOwn(data: MetadataOwner & MetadataManager.Key): boolean {
        return this.of(data).hasNotOwn(data.key);
    }

    public has(data: MetadataOwner & MetadataManager.Key): boolean {
        return this.of(data).has(data.key);
    }
    public hasNot(data: MetadataOwner & MetadataManager.Key): boolean {
        return this.of(data).hasNot(data.key);
    }

    public getOwn(data: MetadataOwner & MetadataManager.Key): any {
        return this.of(data).getOwn(data.key);
    }

    public get(data: MetadataOwner & MetadataManager.Key): any {
        return this.of(data).get(data.key);
    }

    public set(data: MetadataOwner & MetadataManager.Key & MetadataManager.Value): void {
        this.of(data).set(data.key, data.value);
    }

    public delete(data: MetadataOwner & MetadataManager.Key): void {
        this.of(data).delete(data.key);
    }

    public decorate(data: MetadataOwner & MetadataManager.Decorators): void {
        this.of(data).decorate(data.decorators);
    }

    public decorator(callback: (metadata: ClassMetadata) => void): MetadataManager.Decorator {
        return (target: object, propertyKey?: string | symbol, descriptorOrParameterIndex?: PropertyDescriptor | number): void => {
            const parameterIndex: undefined | number = Check.isNumber(descriptorOrParameterIndex)
                ? descriptorOrParameterIndex
                : undefined;
            const metadata: ClassMetadata = this.of({
                target,
                propertyKey,
                parameterIndex,
            });

            callback(metadata);
        };
    }
}
export namespace MetadataManager {
    export interface Key {
        readonly key: string | symbol;
    }

    export interface Value {
        readonly value: unknown;
    }

    export interface Decorators {
        readonly decorators: ReadonlyArray<ClassDecorator | PropertyDecorator | MethodDecorator | ParameterDecorator>;
    }

    export type Decorator = ClassDecorator & PropertyDecorator & MethodDecorator & ParameterDecorator;
}
