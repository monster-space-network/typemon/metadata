import { Check } from '@typemon/check';

import { MetadataOwner } from './metadata-owner';

export class MetadataDoesNotExistError extends Error {
    public constructor(
        public readonly key: string | symbol,
        public readonly owner: MetadataOwner,
    ) {
        super();

        const stringifiedKey: string = Check.isString(this.key)
            ? `String(${this.key})`
            : this.key.toString();

        this.name = this.constructor.name;
        this.message = `Metadata '${stringifiedKey}' does not exist.`;
    }
}
