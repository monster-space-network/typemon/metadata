import { Check } from '@typemon/check';

import { MetadataKey } from './metadata-key';
import { ClassMetadata } from './class-metadata';
import { MetadataDoesNotExistError } from './metadata-does-not-exist.error';

export class ClassParameterMetadata extends ClassMetadata {
    private readonly data: Map<unknown, unknown>;

    public constructor(context: ClassMetadata.Context) {
        super(context);

        this.data = new Map();

        const types: ReadonlyArray<unknown> = Reflect.getOwnMetadata(MetadataKey.DESIGN_PARAMETER_TYPES, this.owner.target, this.owner.propertyKey!) ?? [];
        const type: unknown = types[this.owner.parameterIndex!];

        if (Check.isNotUndefined(type)) {
            this.set(MetadataKey.DESIGN_TYPE, type);
        }
    }

    public hasOwn(key: string | symbol): boolean {
        return this.data.has(key);
    }

    public has(key: string | symbol): boolean {
        return this.data.has(key) || Check.isNotNull(this.parent) && this.parent.has(key);
    }

    public getOwn(key: string | symbol): any {
        if (this.data.has(key)) {
            return this.data.get(key);
        }

        throw new MetadataDoesNotExistError(key, this.owner);
    }

    public get(key: string | symbol): any {
        if (this.data.has(key)) {
            return this.data.get(key);
        }

        if (Check.isNull(this.parent)) {
            throw new MetadataDoesNotExistError(key, this.owner);
        }

        return this.parent.get(key);
    }

    public set(key: string | symbol, value: unknown): void {
        this.data.set(key, value);
    }

    public delete(key: string | symbol): void {
        this.data.delete(key);
    }
}
