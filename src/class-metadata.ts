import { Check } from '@typemon/check';

import { MetadataOwner } from './metadata-owner';
import { MetadataDoesNotExistError } from './metadata-does-not-exist.error';

export class ClassMetadata {
    public readonly owner: MetadataOwner;
    public readonly parent: null | ClassMetadata;

    public constructor(context: ClassMetadata.Context) {
        this.owner = context.owner;
        this.parent = context.parent;
    }

    public hasOwn(key: string | symbol): boolean {
        return Reflect.hasOwnMetadata(key, this.owner.target, this.owner.propertyKey!);
    }
    public hasNotOwn(key: string | symbol): boolean {
        return Check.isFalse(this.hasOwn(key));
    }

    public has(key: string | symbol): boolean {
        return Reflect.hasMetadata(key, this.owner.target, this.owner.propertyKey!);
    }
    public hasNot(key: string | symbol): boolean {
        return Check.isFalse(this.has(key));
    }

    public getOwn(key: string | symbol): any {
        if (this.hasOwn(key)) {
            return Reflect.getOwnMetadata(key, this.owner.target, this.owner.propertyKey!);
        }

        throw new MetadataDoesNotExistError(key, this.owner);
    }

    public get(key: string | symbol): any {
        if (this.has(key)) {
            return Reflect.getMetadata(key, this.owner.target, this.owner.propertyKey!);
        }

        throw new MetadataDoesNotExistError(key, this.owner);
    }

    public set(key: string | symbol, value: unknown): void {
        Reflect.defineMetadata(key, value, this.owner.target, this.owner.propertyKey!);
    }

    public delete(key: string | symbol): void {
        Reflect.deleteMetadata(key, this.owner.target, this.owner.propertyKey!);
    }

    public decorate(decorators: ReadonlyArray<ClassDecorator | PropertyDecorator | MethodDecorator | ParameterDecorator>): void;
    public decorate(decorators: ReadonlyArray<any>): void {
        for (const decorator of decorators) {
            decorator(this.owner.target, this.owner.propertyKey, this.owner.parameterIndex);
        }
    }
}
export namespace ClassMetadata {
    export interface Context {
        readonly owner: MetadataOwner;
        readonly parent: null | ClassMetadata;
    }
}
