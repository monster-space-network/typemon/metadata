export interface MetadataOwner {
    readonly target: object;
    readonly propertyKey?: string | symbol;
    readonly parameterIndex?: number;
}
