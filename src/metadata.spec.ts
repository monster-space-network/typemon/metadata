import { MetadataKey } from './metadata-key';
import { Metadata } from './metadata';
import { MetadataDoesNotExistError } from './metadata-does-not-exist.error';

const EmptyDecorator: Metadata.Decorator = Metadata.decorator((): null => null);

function runDefault(owner: Metadata.Owner): void {
    const metadata: Metadata = Metadata.of(owner);

    expect(metadata.parent).toBeNull();

    const key: string = 'foo';
    const firstValue: unknown = 'bar';
    const secondValue: unknown = 'baz';

    expect(Metadata.hasOwn({ ...owner, key })).toBeFalsy();
    expect(Metadata.hasNotOwn({ ...owner, key })).toBeTruthy();
    expect(Metadata.has({ ...owner, key })).toBeFalsy();
    expect(Metadata.hasNot({ ...owner, key })).toBeTruthy();

    expect((): unknown => Metadata.getOwn({ ...owner, key })).toThrow(MetadataDoesNotExistError);
    expect((): unknown => Metadata.get({ ...owner, key })).toThrow(MetadataDoesNotExistError);

    Metadata.set({ ...owner, key, value: firstValue });

    expect(Metadata.hasOwn({ ...owner, key })).toBeTruthy();
    expect(Metadata.hasNotOwn({ ...owner, key })).toBeFalsy();
    expect(Metadata.has({ ...owner, key })).toBeTruthy();
    expect(Metadata.hasNot({ ...owner, key })).toBeFalsy();

    expect(Metadata.getOwn({ ...owner, key })).toBe(firstValue);
    expect(Metadata.get({ ...owner, key })).toBe(firstValue);

    Metadata.decorate({
        ...owner,
        decorators: [
            Metadata.decorator((metadata: Metadata): void => Metadata.set({ ...metadata.owner, key, value: secondValue })),
        ],
    });

    expect(Metadata.hasOwn({ ...owner, key })).toBeTruthy();
    expect(Metadata.hasNotOwn({ ...owner, key })).toBeFalsy();
    expect(Metadata.has({ ...owner, key })).toBeTruthy();
    expect(Metadata.hasNot({ ...owner, key })).toBeFalsy();

    expect(Metadata.getOwn({ ...owner, key })).toBe(secondValue);
    expect(Metadata.get({ ...owner, key })).toBe(secondValue);

    Metadata.delete({ ...owner, key });

    expect(Metadata.hasOwn({ ...owner, key })).toBeFalsy();
    expect(Metadata.hasNotOwn({ ...owner, key })).toBeTruthy();
    expect(Metadata.has({ ...owner, key })).toBeFalsy();
    expect(Metadata.hasNot({ ...owner, key })).toBeTruthy();

    expect((): unknown => Metadata.getOwn({ ...owner, key })).toThrow(MetadataDoesNotExistError);
    expect((): unknown => Metadata.get({ ...owner, key })).toThrow(MetadataDoesNotExistError);
}

function runExtends(parentOwner: Metadata.Owner, childOwner: Metadata.Owner): void {
    const child: Metadata = Metadata.of(childOwner);
    const parent: Metadata = Metadata.of(parentOwner);

    expect(child.parent).toBe(parent);
    expect(parent.parent).toBeNull();

    const key: symbol = Symbol.for('metadata');
    const parentValue: unknown = 'parent';
    const childValue: unknown = 'child';

    Metadata.set({ ...parentOwner, key, value: parentValue });

    expect(Metadata.hasOwn({ ...childOwner, key })).toBeFalsy();
    expect(Metadata.hasNotOwn({ ...childOwner, key })).toBeTruthy();
    expect(Metadata.has({ ...childOwner, key })).toBeTruthy();
    expect(Metadata.hasNot({ ...childOwner, key })).toBeFalsy();

    expect((): unknown => Metadata.getOwn({ ...childOwner, key })).toThrow(MetadataDoesNotExistError);
    expect(Metadata.get({ ...childOwner, key })).toBe(parentValue);

    Metadata.set({ ...childOwner, key, value: childValue });

    expect(Metadata.hasOwn({ ...childOwner, key })).toBeTruthy();
    expect(Metadata.hasNotOwn({ ...childOwner, key })).toBeFalsy();
    expect(Metadata.has({ ...childOwner, key })).toBeTruthy();
    expect(Metadata.hasNot({ ...childOwner, key })).toBeFalsy();

    expect(Metadata.getOwn({ ...childOwner, key })).toBe(childValue);
    expect(Metadata.get({ ...childOwner, key })).toBe(childValue);
}

test('is', (): void => {
    class Example { }

    const metadata: Metadata = Metadata.of({
        target: Example,
    });

    expect(Metadata.is(metadata)).toBeTruthy();
    expect(Metadata.isNot(metadata)).toBeFalsy();

    expect(Metadata.is(null)).toBeFalsy();
    expect(Metadata.isNot(null)).toBeTruthy();
});

describe('constructor', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example,
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
        }, {
            target: Child,
        });
    });
});

describe('constructor-parameter', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example,
            parameterIndex: 0,
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            parameterIndex: 0,
        }, {
            target: Child,
            parameterIndex: 0,
        });
    });

    test('type', (): void => {
        class Example {
            public constructor(
                @EmptyDecorator
                public readonly parameter: string,
            ) { }
        }

        const metadata: Metadata = Metadata.of({
            target: Example,
            parameterIndex: 0,
        });

        expect(metadata.getOwn(MetadataKey.DESIGN_TYPE)).toBe(String);
    });
});

describe('static-property', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example,
            propertyKey: 'property',
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            propertyKey: 'property',
        }, {
            target: Child,
            propertyKey: 'property',
        });
    });
});

describe('static-method', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example,
            propertyKey: 'method',
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            propertyKey: 'method',
        }, {
            target: Child,
            propertyKey: 'method',
        });
    });
});

describe('static-method-parameter', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example,
            propertyKey: 'method',
            parameterIndex: 0,
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            propertyKey: 'method',
            parameterIndex: 0,
        }, {
            target: Child,
            propertyKey: 'method',
            parameterIndex: 0,
        });
    });

    test('type', (): void => {
        class Example {
            public static method(
                @EmptyDecorator
                parameter: string,
            ): string {
                return parameter;
            }
        }

        const metadata: Metadata = Metadata.of({
            target: Example,
            propertyKey: 'method',
            parameterIndex: 0,
        });

        expect(metadata.getOwn(MetadataKey.DESIGN_TYPE)).toBe(String);
    });
});

describe('instance-property', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example.prototype,
            propertyKey: 'property',
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            propertyKey: 'property',
        }, {
            target: Child,
            propertyKey: 'property',
        });
    });
});

describe('instance-method', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example.prototype,
            propertyKey: 'method',
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            propertyKey: 'method',
        }, {
            target: Child,
            propertyKey: 'method',
        });
    });
});

describe('instance-method-parameter', (): void => {
    test('default', (): void => {
        class Example { }

        runDefault({
            target: Example.prototype,
            propertyKey: 'method',
            parameterIndex: 0,
        });
    });

    test('extends', (): void => {
        class Parent { }

        class Child extends Parent { }

        runExtends({
            target: Parent,
            propertyKey: 'method',
            parameterIndex: 0,
        }, {
            target: Child,
            propertyKey: 'method',
            parameterIndex: 0,
        });
    });

    test('type', (): void => {
        class Example {
            public method(
                @EmptyDecorator
                parameter: string,
            ): string {
                return parameter;
            }
        }

        const metadata: Metadata = Metadata.of({
            target: Example.prototype,
            propertyKey: 'method',
            parameterIndex: 0,
        });

        expect(metadata.getOwn(MetadataKey.DESIGN_TYPE)).toBe(String);
    });
});
