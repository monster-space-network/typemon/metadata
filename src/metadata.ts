import { ClassMetadata } from './class-metadata';
import { MetadataManager } from './metadata-manager';
import { MetadataOwner } from './metadata-owner';

export type Metadata = ClassMetadata;
export const Metadata: MetadataManager = new MetadataManager();
export namespace Metadata {
    export type Owner = MetadataOwner;
    export type Key = MetadataManager.Key;
    export type Value = MetadataManager.Value;
    export type Decorator = MetadataManager.Decorator;
}
