export namespace MetadataKey {
    export const DESIGN_TYPE: string = 'design:type';
    export const DESIGN_PARAMETER_TYPES: string = 'design:paramtypes';
    export const DESIGN_RETURN_TYPE: string = 'design:returntype';

    export const METADATA: string = 'typemon.metadata';
    export const METADATA_LIST: string = 'typemon.metadata.list';
}
